#include <stdio.h>
#include <stdlib.h>

int main()
{

    unsigned short int variablea = 5;
    unsigned short int variableb = 6;
    unsigned short int variablec = variablea - variableb;
    printf("%u\n", variablec);

    unsigned int variabled = 5;
    unsigned int variablee = 6;
    unsigned int variablef = variabled - variablee;
    printf("%u\n", variablef);

    unsigned long int variableg = 5;
    unsigned long int variableh = 6;
    unsigned long int variablei = variableg - variableh;
    printf("%u\n", variablei);

    int xx = sizeof(unsigned short int);
    int yy = sizeof(unsigned int);
    int zz = sizeof(unsigned long int);

    printf("%u\n", xx);
    printf("%u\n", yy);
    printf("%u\n", zz);

    return 0;

}
